% Cloud for dumbies
% 17/05/2016
% Flavien Hardy & Ludovic Logiou

# Cloud

### Selon vous ?
![](./images/futuramafry.jpg)

----

### Définition
Le cloud computing ou l’informatique en nuage ou nuagique ou encore l’infonuagique (au Québec), est l'exploitation de la puissance de calcul ou de stockage de serveurs informatiques distants par l'intermédiaire d'un réseau, généralement internet.


# Public / Privée / Communautaire ?


### Cloud Public
* Amazon Web Service
* Azure
* CloudWatt
* Google Compute Engine / Google Kubernete Engine
* Rack Space

----

### Cloud Privé
* Openstack
* Cloudstack
* OpenNebula

----

### Communautaire
* Cloud partagé dans une communauté
* Par exemple : Un cloud universitaire

### Les enjeux
* Données (Patriot Act...)
* Coûts
* Compétences
* Scalabilité

----

### ![](./images/scalability.png)


# Pizza As A Service

### ![](./images/pizza.jpg)

### Infrastructure as a Service (IaaS)

* Environnement entièrement configurable
* AWS, OpenStack et autres solutions Cloud
* Autoscalling, LBaaS, VPNaaS, DNSaaS, DBaaS...

----

### Platform as a Service (PaaS)

* Plateforme sur laquelle des développeurs ou éditeurs de logiciels peuvent déployer des applications
* Mise à disposition d'un repo (exemple: un repos git)
* OpenShift (By Redhat), OpenShift Origin,  AWS Elastic Beanstalk

----

### Software as a Service (SaaS)

* Dropbox, Office 365, Gmail, Bluemind SAAS...
* Mise à disposition d'un service
* Le client n'a pas à se sousier de l'infrastructure derrière ni des MàJ

----

###![](./images/iaas-paas-saas.png)


# DevOPS et Cloud ?

### Test automation CI/CD
* Déployer une VM pour y effectuer des jeux de tests
* Avoir des plateformes de Développement et Preprod totalement ISO à la production

----

### Industrialisation
* Industrialisation total de son infrastructure  
* Provisionnement des serveurs 
* Déploiement des applications 
* Notion de rollback via les snapshots

----

### Intégration continue (Atlas By HashiCorp)

![](./images/atlas.png)


# Labo OpenStack de la BU

### Pourquoi ?
* Montée en compétences
* Pouvoir tester rapidement des technologies 
* Monter aisément des environnements de test

----

### Compliqué ?
* Beaucoup de briques à appréhender (Réseau, Virtualisation...)
* Une nouvelle façon de penser un réseau

----

### Ce qui n'a pas été mis en place
* Backend de stockage (Ceph, SAN)
* DBaaS (DataBase as a Service), LBaaS, VPNaaS
* Haute disponibilité


# Des questions ?


