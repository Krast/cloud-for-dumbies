#!/usr/bin/env make
OUTPUTFILE	= index.html
INPUTFILE	= cloud-for-dumbies.md
all::
	pandoc -s -i -t revealjs ${INPUTFILE} -o ${OUTPUTFILE}

clean::
	rm -f ${OUTPUTFILE}
