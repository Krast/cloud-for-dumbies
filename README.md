# Requirments
* Pandoc

# Modifier les slide 
Il suffit d'éditer le fichier cloud-for-dumbies.md

# Générer les slides
```bash
make clean && make
```

# Des images
IL faut placer toutes les images du Slide dans le répertoire ./images
